<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Interview Add</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
	<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/index4style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/fonts/*">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
	<style>
	#small-font
	{
	Font-size:16px;
	}
	</style>
	<%@ include file="AdminDashboard.jsp" %>
</head>
<body>
<div class="col-md-9">
<div class="one"  style="margin-left:100px;margin-right:100px;">
<div class="panel">
<div class="panel-body" id="small-font">
<form method="POST" action="${pageContext.request.contextPath}/AddInterview">
						<h1>Interview</h1>
						<div style="width: 115px; padding-top: 10px; float: left"
							align="left">Interview Title:</div>
						<div style="width: 300px; float: left;" align="left">
							<input type="text" name="title" required id="title" value=""
								class="vpb_textAreaBoxInputs">
						</div>
						<br clear="all">
						<br clear="all">
						
						<br clear="all">
						<br clear="all">
						<div style="width: 115px; padding-top: 10px; float: left"
							align="left">Interview Description:</div>
						<div style="border:1px solid #6CF ;float: left;" align="left">
							<textarea rows="15" cols="80" name="description" required id="description"
								value=""></textarea>
								</div>
						<br clear="all">
						<br clear="all">
						<div style="width: 115px; padding-top: 10px; float: left"
							align="left">Editor:</div>
						<div style="width: 300px;; float: left;" align="left">
							<input type="text" name="editor" required id="editor" value=""
								class="vpb_textAreaBoxInputs">
						</div>
						<br clear="all">
						<br clear="all">
						<button class="btn btn-primary">Add</button>
						<button class="btn btn-primary" style="margin-left:100px">Cancel</button>
				</form>
					</div>
					</div>
					</div>
					</div>
</body>
</html>