<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/fonts/*">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>

<title>Admin Dashboard</title>

<style>
limit {
  display: inline-block;
  position: relative;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
</head>

<body>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Admin Dashboard</a>
		</div>
		<ul class="nav navbar-nav navbar-right">


			<li><a href="" data-toggle="modal"
				data-target=".bs-example-modal-lg"><span
					class="glyphicon glyphicon-user">AddAdmin</span></a></li>
			<li><a href="${pageContext.request.contextPath}/login"><span class="glyphicon glyphicon-off">Logout</span></a></li>
		</ul>
	</div>
	</nav>

	<div class="container">
		<center>
			<div class="modal fade bs-example-modal-lg" tabindex="-1"
				role="dialog" aria-labelledby="userregistration">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="model-header"
							style="padding: 30px 30px; background-color: black">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 align="left" style="margin-top: 0px; color: white"
								class="model-title">Admin registration</h4>
						</div>
						<div class="model-body" style="padding: 40px 50px;">
							<form action="admininfo" method="POST">
								<div style="width: 115px; padding-top: 10px; float: left"
									align="left">Username:</div>
								<div style="width: 300px; float: left;" align="left">
									<input type="text" name="username" required id="username"
										value="" class="vpb_textAreaBoxInputs">
								</div>
								<br clear="all">
								<br clear="all">
								<div style="width: 115px; padding-top: 10px; float: left"
									align="left">Email:</div>
								<div style="width: 300px; float: left;" align="left">
									<input type="text" name="email" required id="email" value=""
										class="vpb_textAreaBoxInputs">
								</div>
								<br clear="all">
								<br clear="all">

								<div style="width: 115px; padding-top: 10px; float: left;"
									align="left">Password:</div>
								<div style="width: 300px; float: left;" align="left">
									<input type="password" name="password" required id="password"
										value="" class="vpb_textAreaBoxInputs">
								</div>
								<br clear="all">
								<br clear="all">

								<!-- <div style="width: 115px; padding-top: 10px; float: left;"
									align="left">User-Type:</div>
								<div style="width: 300px; float: left;" align="left">
									<select name="usertype" id="usertype">
										<option value="Admin">Admin</option>
										<option value="Editor">Editor</option>

									</select>
								</div> -->
								<br clear="all">
								<br clear="all">


								<div style="width: 300px; float: left;" align="left">
									<input type="submit" name="submit" id="submit" value="Register"
										style="margin-right: 50px;" class="vpb_general_button_g">
									<a href="AdminDashboard.jsp" style="text-decoration: none;"
										class="vpb_general_button_g">Cancel</a>
							</form>
						</div>
					</div>
					<br />
					<br />
					<div class="model-footer"
						style="height: 50px; background-color: black"></div>
				</div>
			</div>
	</div>
	</center>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="nav-side-menu">
				<div class="brand">
					<img
						src="${pageContext.request.contextPath}/static/images/logo.jpg"
						style="margin-left: 0; height: 100px; width: 100px"> <br />${loggedInUser}<br />
				</div>
				<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse"
					data-target="#menu-content"></i>

				<div class="menu-list">

					<ul id="menu-content" class="menu-content collapse out">
						

						<li><a href="#"> <i class="fa fa-users fa-lg"></i>Home
						</a></li>
						<li data-toggle="collapse" data-target="#products"
							class="collapsed"><a href="#"><i
								class="fa fa-gift fa-lg"></i>All Posts <span class=glyphicon glyphicon-chevron-down></span></a>
						</li>
						<ul class="sub-menu collapse" id="products">
							<li><a  href="${pageContext.request.contextPath }/NewsList">News</a></li>
							<li><a  href="${pageContext.request.contextPath }/EventsList">Events</a></li>
							<li><a  href="${pageContext.request.contextPath }/InterviewList">Interview</a></li>
							<li><a  href="${pageContext.request.contextPath }/JobList">Job</a></li>
							<li><a  href="${pageContext.request.contextPath }/BlogList">Blog</a></li>
							<li><a  href="${pageContext.request.contextPath }/GadgetsList">Gadgets</a></li>
							<li><a  href="${pageContext.request.contextPath }/VideoList"><span class="glyphicon glyphicon-play-circle">Video</span></a></li>
						</ul>

						<li><a  href="#user"> <i
								class="fa fa-users fa-lg"></i>admins
						</a></li>
					</ul>
				</div>
			</div>
		</div>
		
	
</body>



</html>