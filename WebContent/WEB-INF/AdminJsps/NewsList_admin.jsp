<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">



<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/style.css">
<title>Insert title here</title>
<%@ include file="AdminDashboard.jsp" %>
</head>
<body>
<div class="col-md-8">
	<h3>Current News</h3><br/>
	<a href="${pageContext.request.contextPath }/NewsAdd">
					<button class="btn btn-primary"><span class="glyphicon glyphicon-plus">AddNews</span></button></a>
					<br/><br/>
	 
             <table class="table table-bordered" style="table-layout:fixed">
						<thead>
						<tr>
						<th>ID</th>
						<th>Title</th>
						<th>description</th>
						<th>Editor</th>
						<th>Date</th>
						<th>Action</th>
						</tr>
						</thead>
					<c:forEach var="row" items="${news}">
						<tbody>
						<tr>
						<td>${row.id}</td>
						<td><limit> ${row.title}</limit></td>
						<td> <limit>${row.description}</limit></td>
						<td> ${row.editor}</td>
						<td> ${row.date}</td>
						<td><a href="<c:url value='/ViewNewsDetails-${row.id}-news' />"><button class="btn btn-primary">View Details</button></a></td>
						</tr>
						</tbody>
					</c:forEach>
					</table>
					</div>
	

</body>
</html>