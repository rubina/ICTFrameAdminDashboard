<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
<title>job Details</title>
</head>
<body>
	  <a href="AdminDashboard"><button class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left">Back</span></button></a><br/>
	<br/><div class="row">
	<div class="col-sm-8">
 <div class="panel"> 
 <div class="panel-header" style="background-color:Dodgerblue"> 
  <h4>   ${job.title} </h4><br />
    </div>
    <div class="panel-body">
     <img src="${pageContext.request.contextPath}/static/images/${job.photo.name}" class="img-responsive"><br/>
     ${job.description}<br />
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${job.editor}</span>
     Date: ${job.date}
      </div>
       </div>
      <a  href="<c:url value='/editJob-${job.id}-job' />"><button class="btn btn-primary"><span class="glyphicon glyphicon-pencil">Edit</span></button></a>
      
       <a onclick="return confirm('Do you want to premanantly delete this news?')" href="<c:url value='/deleteJob-${job.id}-job' />"><button class="btn btn-danger"><span class="glyphicon glyphicon-trash">Delete</span></button></a><br/>
	</div>
	</div>
</body>
</html>

