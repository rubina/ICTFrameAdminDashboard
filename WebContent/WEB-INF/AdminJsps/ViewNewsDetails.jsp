<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
<title>News Details</title>
</head>
<body>
	   <a href="AdminDashboard" ><button class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left">Back</span></button></a><br/>
	<br/><div class="row">
	<div class="col-sm-8">
 <div class="panel"> 
 <div class="panel-header" style="background-color:Dodgerblue"> 
  <h4>   ${news.title} </h4><br />
    </div>
    <div class="panel-body">
    <img src="${pageContext.request.contextPath}/static/images/${news.photo.name}" class="img-responsive"><br/>
     ${news.description}<br />
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${news.editor}</span>
     Date: ${news.date}
      </div>
       </div>
     <a href="<c:url value='/editNews-${news.id}-news' />"><button class="btn btn-primary"><span class="glyphicon glyphicon-pencil">Edit</span></button></a>
       <a onclick="return confirm('Do you want to premanantly delete this news?')" href="<c:url value='/deleteNews-${news.id}-news' />"><button class="btn btn-danger"><span class="glyphicon glyphicon-trash">Delete</span></button></a><br/>
	</div>
	</div>
</body>
</html>

