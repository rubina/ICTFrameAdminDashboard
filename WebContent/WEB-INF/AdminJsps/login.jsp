<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="Stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<title>Insert title here</title>
</head>

<body onload='document.f.j_username.focus();'
	background="${pageContext.request.contextPath}/static/images/image.jpg">
	<center style="margin-top:200px;margin-left:550px">
<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: Grey">
					<h3 class="panel-title">
						<strong> Admin Login </strong>
					</h3>
				</div>
				<c:if test="${param.error!=null}">
				<p>Login fail check username and password</p>
				</c:if>
				<div class="panel-body center-block text-justify" >
					<form  name=f action="${pageContext.request.contextPath}/j_spring_security_check"method="POST">
					
						<div class="form-group">
							<label for="username">Username</label> <input
								type="text" name="j_username"class="form-control" id="username"
								placeholder="Enter username">
						</div>
						
						<div class="form-group">
							<label for="Password">Password <a
								href="/sessions/forgot_password">(forgot password)</a></label> <input
								type="password" name="j_password" class="form-control" id="Password"
								placeholder="Password">
						</div>
						<button type="submit" class="btn btn-sm btn-primary">login
							</button>
					</form>
				</div>
			</div>
			</div>
</center>

</body>
</html>