package com.softech.ictframe.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.Blog;

@Transactional
@Component
public class BlogDAO {
	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveBlog(Blog blog)
	{
		Transaction trans=session().beginTransaction();
		session().save(blog);
		trans.commit();
	}
	public List<Blog> GetAllBlog()
	{
		Transaction trans=session().beginTransaction();
		List<Blog> blog=session().createQuery("from Blog order by id desc").list();
		trans.commit();
		return blog;
	}
	public void DeleteBlog(Blog blog)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from Blog where blog_id="+blog.getId()).executeUpdate();
		trans.commit();
		}
	public Blog GetSpecificBlog(int id)
	{
		Transaction trans=session().beginTransaction();
		Blog blog=(Blog)session().get(Blog.class,id);
		trans.commit();
		return blog;
	}
	public void UpdateBlog(Blog blog)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(blog);
		trans.commit();
		
	}

}
