package com.softech.ictframe.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.Interview;

@Transactional 
@Component
public class InterviewDAO {


	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveInterview(Interview interview)
	{
		Transaction trans=session().beginTransaction();
		session().save(interview);
		trans.commit();
	}
	public List<Interview> GetAllInterview()
	{
		Transaction trans=session().beginTransaction();
		List<Interview> interview=session().createQuery("from Interview order by id desc").list();
		trans.commit();
		return interview;
	}
	public void DeleteInterview(Interview interview)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from Interview where Interview_id="+interview.getId()).executeUpdate();
		trans.commit();
		}
	public Interview GetSpecificInterview(int id)
	{
		Transaction trans=session().beginTransaction();
		Interview interview=(Interview)session().get(Interview.class,id);
		trans.commit();
		return interview;
	}
	public void UpdateInterview(Interview interview)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(interview);
		trans.commit();
		
	}

}
