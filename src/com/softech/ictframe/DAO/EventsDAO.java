package com.softech.ictframe.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.Events;
@Transactional
@Component
public class EventsDAO {
	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveEvents(Events events)
	{
		Transaction trans=session().beginTransaction();
		session().save(events);
		trans.commit();
	}
	public List<Events> GetAllEvents()
	{
		Transaction trans=session().beginTransaction();
		List<Events> events=session().createQuery("from Events order by id desc").list();
		trans.commit();
		return events;
	}
	public void DeleteEvents(Events events)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from Events where events_id="+events.getId()).executeUpdate();
		trans.commit();
		}
	public Events GetSpecificEvents(int id)
	{
		Transaction trans=session().beginTransaction();
		Events events=(Events)session().get(Events.class,id);
		trans.commit();
		return events;
	}
	public void UpdateEvents(Events events)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(events);
		trans.commit();
		
	}
	
}
