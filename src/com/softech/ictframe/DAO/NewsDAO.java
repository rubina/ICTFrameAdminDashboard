package com.softech.ictframe.DAO;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.News;
@Transactional
@Component
public class NewsDAO {
	@Autowired 
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveNews(News news)
	{
		Transaction trans=session().beginTransaction();
		session().save(news);
		trans.commit();
	}
	public List<News> GetAllNews()
	{
		Transaction trans=session().beginTransaction();
		List<News> news=session().createQuery("from News order by id desc").list();
		trans.commit();
		return news;
	}
	public void Delete(News news)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from News where news_id="+news.getId()).executeUpdate();
		trans.commit();
	}
	public News GetSpecificNews(int id)
	{
		Transaction trans=session().beginTransaction();
		News news=(News) session().get(News.class,id);
		trans.commit();
		return news;
	}
	public void UpdateNews(News news)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(news);
		trans.commit();
	}
	

}
