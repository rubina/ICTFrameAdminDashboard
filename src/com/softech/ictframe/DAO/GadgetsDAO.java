package com.softech.ictframe.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.Gadgets;

@Transactional 
@Component
public class GadgetsDAO {


	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveGadgets(Gadgets gadgets)
	{
		Transaction trans=session().beginTransaction();
		session().save(gadgets);
		trans.commit();
	}
	public List<Gadgets> GetAllGadgets()
	{
		Transaction trans=session().beginTransaction();
		List<Gadgets> gadgets=session().createQuery("from Gadgets order by id desc").list();
		trans.commit();
		return gadgets;
	}
	public void DeleteGadgets(Gadgets gadgets)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from Gadgets where Gadgets_id="+gadgets.getId()).executeUpdate();
		trans.commit();
		}
	public Gadgets GetSpecificGadgets(int id)
	{
		Transaction trans=session().beginTransaction();
		Gadgets gadgets=(Gadgets)session().get(Gadgets.class,id);
		trans.commit();
		return gadgets;
	}
	public void UpdateGadgets(Gadgets gadgets)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(gadgets);
		trans.commit();
		
	}



}
