package com.softech.ictframe.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.Job;
@Transactional 
@Component
public class JobDAO {
	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveJob(Job job)
	{
		Transaction trans=session().beginTransaction();
		session().save(job);
		trans.commit();
	}
	public List<Job> GetAllJob()
	{
		Transaction trans=session().beginTransaction();
		List<Job> job=session().createQuery("from Job order by id desc").list();
		trans.commit();
		return job;
	}
	public void DeleteJob(Job job)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from Job where job_id="+job.getId()).executeUpdate();
		trans.commit();
		}
	public Job GetSpecificJob(int id)
	{
		Transaction trans=session().beginTransaction();
		Job job=(Job)session().get(Job.class,id);
		trans.commit();
		return job;
	}
	public void UpdateJob(Job job)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(job);
		trans.commit();
		
	}

}
