package com.softech.ictframe.DAO;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ictframe.DAOModels.Video;
@Transactional
@Component
public class VideoDAO {
	@Autowired 
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveVideo(Video Video)
	{
		Transaction trans=session().beginTransaction();
		session().save(Video);
		trans.commit();
	}
	public List<Video> GetAllVideo()
	{
		Transaction trans=session().beginTransaction();
		List<Video> Video=session().createQuery("from Video order by id desc").list();
		trans.commit();
		return Video;
	}
	public void Delete(Video Video)
	{
		Transaction trans=session().beginTransaction();
		session().createQuery("Delete from Video where Video_id="+Video.getId()).executeUpdate();
		trans.commit();
	}
	public Video GetSpecificVideo(int id)
	{
		Transaction trans=session().beginTransaction();
		Video Video=(Video) session().get(Video.class,id);
		trans.commit();
		return Video;
	}
	public void UpdateVideo(Video Video)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(Video);
		trans.commit();
	}
	

}
