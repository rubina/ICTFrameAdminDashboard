package com.softech.ictframe.DAOModels;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Blog")
public class Blog {
	@Id
	@GeneratedValue
	@Column(name="blog_id")
	private int id;
	@Column(name="blog_title")
	private String title;
	@Column(name="blog_description",columnDefinition="LONGTEXT")
	private String description;
	@Column(name="blog_date",columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private String date;
	@Column(name="blog_editor")
	private String editor;
	
	@OneToOne(cascade = CascadeType.ALL)
	private BlogPhoto photo;
	public Blog()
	{}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public BlogPhoto getPhoto() {
		return photo;
	}
	public void setPhoto(BlogPhoto photo) {
		this.photo = photo;
	}
	

}
