package com.softech.ictframe.DAOModels;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Events")
public class Events {
	@Id
	@GeneratedValue
	@Column(name="events_id")
	private int id;
	@Column(name="events_title")
	private String title;
	@Column(name="events_description",columnDefinition="LONGTEXT")
	private String description;
	@Column(name="events_date",columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private String date;
	@Column(name="events_editor")
	private String editor;
	@OneToOne(cascade = CascadeType.ALL)
	private EventsPhoto photo;
	
	public Events()
	{
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public EventsPhoto getPhoto() {
		return photo;
	}
	public void setPhoto(EventsPhoto photo) {
		this.photo = photo;
	}
	

}
