package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.Gadgets;
import com.softech.ictframe.DAOModels.GadgetsPhoto;
import com.softech.ictframe.DAOModels.Interview;
import com.softech.ictframe.DAOModels.InterviewPhoto;
import com.softech.ictframe.services.InterviewService;

@Controller
public class InterviewController {
	@Autowired
	private InterviewService interviewService;
	public void setInterviewService(InterviewService interviewsservice)
	{
	interviewService=interviewsservice;	
	}
	@RequestMapping("/InterviewList")
	public String showInterviewList(Model model)
	{
	List<Interview> interviews=interviewService.GetAllInterview();
	model.addAttribute("interview", interviews);
	return "InterviewList_admin";
	}
	@RequestMapping(value = { "/ViewInterviewDetails-{id}-interview" }, method = RequestMethod.GET)
	public String showInterviewDetails(Model model,@PathVariable int id,Interview interview)
	{
		interview=interviewService.GetSpecificInterview(id);
		model.addAttribute("interview",interview);
		return "ViewInterviewDetails";
	}

	@RequestMapping(value = { "/editInterview-{id}-interview" }, method = RequestMethod.GET)
	public String showInterviewToEdit(Model model,@PathVariable int id,Interview interview)
	{
		interview=interviewService.GetSpecificInterview(id);
		model.addAttribute("interview",interview);
		return "EditInterview_admin";
	}

	@RequestMapping(value = { "/editInterview-{id}-interview" }, method = RequestMethod.POST)
	public String showUpdateclass(Model model,Interview interview,@PathVariable int id)
	{
		interviewService.UpdateInterview(interview,id);
		return "ViewInterviewDetails";
	}
	@RequestMapping(value = { "/deleteInterview-{id}-interview" })
	public String showDeleteClass(Interview interview)
	{
		interviewService.DeleteInterviewById(interview);
		
		return "redirect:/InterviewList";
	}
	@RequestMapping("/InterviewAdd")
	public String showInterviewAdd()
	{
	return "InterviewAdd_admin";	
	}
	@RequestMapping("/AddInterview")
	public String showAddedInterview(Interview interview,Model model)
	{
		interviewService.SaveInterview(interview);
		int id=interview.getId();
		model.addAttribute("id",id);
		return "InterviewPhoto";
	}
	@RequestMapping(value = "/uploadInterviewphoto")
	public String uploadInterviewphoto(HttpServletRequest request, @RequestParam CommonsMultipartFile[] photoUpload,
		 @RequestParam("id") int id, Model model, Interview interview) throws Exception, IOException {

		String saveDirectory = "D:\\Spring\\ICTFrameAdminHibernate\\WebContent\\Resources\\images\\";
		String fileName = "";
		String fileLocation = "";	

		if (photoUpload != null && photoUpload.length > 0) {
			for (CommonsMultipartFile aFile : photoUpload) {

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
					fileName = aFile.getOriginalFilename();
					fileLocation = saveDirectory;
				}
			}
		}
		Interview persistentinterview = interviewService.GetSpecificInterview(id);
		
		
		if (photoUpload != null && photoUpload.length > 0) {
		InterviewPhoto photo = new InterviewPhoto(fileName, fileLocation);
		persistentinterview.setPhoto(photo);
		}
		
		interviewService.updateinterview(persistentinterview);

		return "redirect:/InterviewList";
	}
	


}
