package com.softech.ictframe.controller;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.softech.ictframe.DAOModels.Events;
import com.softech.ictframe.services.EventsService;
import com.softech.ictframe.services.NewsService;

@Controller
public class HomeController {
	@Autowired
	private EventsService eventservice;
	public void setEventsService(EventsService eventsservice)
	{
		eventservice=eventsservice;
	}
@RequestMapping(value= {"/","AdminDashboard"})
public String showAdminLogin(Model model,Events events)
{
	
	
return "AdminDashboard";	
}
/*@RequestMapping("/login")
public String showDashboard()
{
return "AdminDashboard";	
}*/
@RequestMapping("/login")
public String showLogin()
{
return "login";	
}
}
