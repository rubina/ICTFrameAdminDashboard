package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.Video;
import com.softech.ictframe.services.VideoService;


@Controller
public class VideoController {
@Autowired
private VideoService videoservice;
public void setvideoservice(VideoService Videosservice)
{
videoservice=Videosservice;	
}
@RequestMapping("/VideoList")
public String showVideoList(Model model)
{
List<Video> Videos=videoservice.GetAllVideo();
model.addAttribute("video", Videos);
return "VideoList_admin";
}
@RequestMapping(value = { "/ViewVideoDetails-{id}-video" }, method = RequestMethod.GET)
public String showVideoDetails(Model model,@PathVariable int id,Video videos)
{
	videos=videoservice.GetSpecificVideo(id);
	model.addAttribute("video",videos);
	return "ViewVideoDetails";
}

@RequestMapping(value = { "/editVideo-{id}-video" }, method = RequestMethod.GET)
public String showVideoToEdit(Model model,@PathVariable int id,Video Video)
{
	Video=videoservice.GetSpecificVideo(id);
	model.addAttribute("video",Video);
	return "EditVideo_admin";
}

@RequestMapping(value = { "/editVideo-{id}-video" }, method = RequestMethod.POST)
public String showUpdateclass(Model model,Video Video,@PathVariable int id)
{
	videoservice.UpdateVideo(Video,id);
	return "ViewVideoDetails";
}
@RequestMapping(value = { "/deleteVideo-{id}-video" })
public String showDeleteClass(Video Video)
{
	videoservice.DeleteClassById(Video);
	
	return "redirect:/VideoList";
}
@RequestMapping("/VideoAdd")
public String showVideoAdd()
{
return "VideoAdd_admin";	
}
@RequestMapping("/AddVideo")
public String showAddedNew(Model model,Video video)
{
videoservice.SaveVideo(video);	
List<Video> videos=videoservice.GetAllVideo();
model.addAttribute("video",videos);
return "VideoList_admin";
}


}
