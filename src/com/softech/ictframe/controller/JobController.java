package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.Interview;
import com.softech.ictframe.DAOModels.InterviewPhoto;
import com.softech.ictframe.DAOModels.Job;
import com.softech.ictframe.DAOModels.JobPhoto;
import com.softech.ictframe.services.JobService;
@Controller
public class JobController {
	@Autowired
	private JobService jobService;
	public void setJobService(JobService jobsservice)
	{
	jobService=jobsservice;	
	}
	@RequestMapping("/JobList")
	public String showJobList(Model model)
	{
	List<Job> jobs=jobService.GetAllJob();
	model.addAttribute("job", jobs);
	return "JobList_admin";
	}
	@RequestMapping(value = { "/ViewJobDetails-{id}-job" }, method = RequestMethod.GET)
	public String showJobDetails(Model model,@PathVariable int id,Job job)
	{
		job=jobService.GetSpecificJob(id);
		model.addAttribute("job",job);
		return "ViewJobDetails";
	}

	@RequestMapping(value = { "/editJob-{id}-job" }, method = RequestMethod.GET)
	public String showJobToEdit(Model model,@PathVariable int id,Job job)
	{
		job=jobService.GetSpecificJob(id);
		model.addAttribute("job",job);
		return "EditJob_admin";
	}

	@RequestMapping(value = { "/editJob-{id}-job" }, method = RequestMethod.POST)
	public String showUpdateclass(Model model,Job job,@PathVariable int id)
	{
		jobService.UpdateJob(job,id);
		return "ViewJobDetails";
	}
	@RequestMapping(value = { "/deleteJob-{id}-job" })
	public String showDeleteClass(Job job)
	{
		jobService.DeleteJobById(job);
		
		return "redirect:/JobList";
	}
	@RequestMapping("/JobAdd")
	public String showJobAdd()
	{
	return "JobAdd_admin";	
	}
	@RequestMapping("/AddJob")
	public String showAddedJob(Job job,Model model)
	{
		jobService.SaveJob(job);
		int id=job.getId();
		model.addAttribute("id",id);
		return "JobPhoto";
	}
	@RequestMapping(value = "/uploadJobphoto")
	public String uploadJobphoto(HttpServletRequest request, @RequestParam CommonsMultipartFile[] photoUpload,
		 @RequestParam("id") int id, Model model, Job job) throws Exception, IOException {

		String saveDirectory = "D:\\Spring\\ICTFrameAdminHibernate\\WebContent\\Resources\\images\\";
		String fileName = "";
		String fileLocation = "";	

		if (photoUpload != null && photoUpload.length > 0) {
			for (CommonsMultipartFile aFile : photoUpload) {

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
					fileName = aFile.getOriginalFilename();
					fileLocation = saveDirectory;
				}
			}
		}
		Job persistentjob = jobService.GetSpecificJob(id);
		
		
		if (photoUpload != null && photoUpload.length > 0) {
		JobPhoto photo = new JobPhoto(fileName, fileLocation);
		persistentjob.setPhoto(photo);
		}
		
		jobService.updatejob(persistentjob);

		return "redirect:/JobList";
	}
	
}
