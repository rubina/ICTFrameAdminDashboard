package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.Events;
import com.softech.ictframe.DAOModels.EventsPhoto;
import com.softech.ictframe.services.EventsService;

@Controller
public class EventsController {
	@Autowired
	private EventsService eventsService;
	public void setEventsService(EventsService eventsservice)
	{
		eventsService=eventsservice;
	}
	@RequestMapping("/EventsList")
	public String showEventList(Model model)
	{
	List<Events> events=eventsService.GetAllEvents();
	model.addAttribute("events", events);
	return "EventsList_admin";
	}
	@RequestMapping(value = { "/ViewEventsDetails-{id}-event" }, method = RequestMethod.GET)
	public String showEventsDetails(Model model,@PathVariable int id,Events events)
	{
		events=eventsService.GetSpecificEvents(id);
		model.addAttribute("event",events);
		return "ViewEventDetails";
	}

	@RequestMapping(value = { "/editEvents-{id}-events" }, method = RequestMethod.GET)
	public String showEventsToEdit(Model model,@PathVariable int id,Events event)
	{
		event=eventsService.GetSpecificEvents(id);
		model.addAttribute("event",event);
		return "EditEvent_admin";
	}

	@RequestMapping(value = { "/editEvents-{id}-events" }, method = RequestMethod.POST)
	public String showUpdateBLog(Model model,Events event,@PathVariable int id)
	{
		eventsService.UpdateEvents(event,id);
		return "ViewEventDetails";
	}
	@RequestMapping(value = { "/deleteEvents-{id}-events" })
	public String showDeleteEvents(Events event)
	{
		eventsService.DeleteEventsById(event);
		
		return "redirect:/EventsList";
	}
	@RequestMapping("/EventsAdd")
	public String showEventAdd()
	{
	return "EventAdd_admin";	
	}
	@RequestMapping("/AddEvent")
	public String showAddedEvent(Events event,Model model)
	{
		eventsService.SaveEvents(event);
		int id=event.getId();
		model.addAttribute("id",id);
		return "Eventsphoto";
	}
	
	@RequestMapping(value = "/uploadEventsphoto")
	public String uploadEventphoto(HttpServletRequest request, @RequestParam CommonsMultipartFile[] photoUpload,
		 @RequestParam("id") int id, Model model, Events events) throws Exception, IOException {

		String saveDirectory = "D:\\Spring\\ICTFrameAdminHibernate\\WebContent\\Resources\\images\\";
		String fileName = "";
		String fileLocation = "";	

		if (photoUpload != null && photoUpload.length > 0) {
			for (CommonsMultipartFile aFile : photoUpload) {

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
					fileName = aFile.getOriginalFilename();
					fileLocation = saveDirectory;
				}
			}
		}
		Events persistentevents = eventsService.GetSpecificEvents(id);
		
		
		if (photoUpload != null && photoUpload.length > 0) {
		EventsPhoto photo = new EventsPhoto(fileName, fileLocation);
		persistentevents.setPhoto(photo);
		}
		
		eventsService.updateevents(persistentevents);

		return "redirect:/EventsList";
	}
	

}
