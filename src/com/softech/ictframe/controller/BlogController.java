package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.Blog;
import com.softech.ictframe.DAOModels.BlogPhoto;
import com.softech.ictframe.services.BlogService;

@Controller
public class BlogController {
	@Autowired
	private BlogService blogservice;
	public void setBlogService(BlogService blogsservice)
	{
		blogservice=blogsservice;
	}
	@RequestMapping("/BlogList")
	public String showBlogList(Model model)
	{
	List<Blog> blog=blogservice.GetAllBlog();
	model.addAttribute("blog", blog);
	return "BlogList_admin";
	}
	@RequestMapping(value = { "/ViewBlogDetails-{id}-blog" }, method = RequestMethod.GET)
	public String showBlogDetails(Model model,@PathVariable int id,Blog blog)
	{
		blog=blogservice.GetSpecificBlog(id);
		model.addAttribute("blog",blog);
		return "ViewBlogDetails";
	}

	@RequestMapping(value = { "/editBlog-{id}-blog" }, method = RequestMethod.GET)
	public String showBlogToEdit(Model model,@PathVariable int id,Blog blog)
	{
		blog=blogservice.GetSpecificBlog(id);
		model.addAttribute("blog",blog);
		return "EditBlog_admin";
	}

	@RequestMapping(value = { "/editBlog-{id}-blog" }, method = RequestMethod.POST)
	public String showUpdateBLog(Model model,Blog blog,@PathVariable int id)
	{
		blogservice.UpdateBlog(blog,id);
		return "ViewBlogDetails";
	}
	@RequestMapping(value = { "/deleteBlog-{id}-blog" })
	public String showDeleteBlog(Blog blog)
	{
		blogservice.DeleteBlogById(blog);
		
		return "redirect:/BlogList";
	}
	@RequestMapping("/BlogAdd")
	public String showBlogAdd()
	{
	return "BlogAdd_admin";	
	}
	@RequestMapping("/AddBlog")
	public String showAddedBlog(Blog blog,Model model)
	{
		blogservice.SaveBlog(blog);
		int id=blog.getId();
		model.addAttribute("id",id);
		return "BlogPhoto";
	}
	
	@RequestMapping(value = "/uploadBlogphoto")
	public String uploadBlogphoto(HttpServletRequest request, @RequestParam CommonsMultipartFile[] photoUpload,
		 @RequestParam("id") int id, Model model, Blog blog) throws Exception, IOException {

		String saveDirectory = "D:\\Spring\\ICTFrameAdminHibernate\\WebContent\\Resources\\images\\";
		
		
		
		String fileName = "";
		String fileLocation = "";
		

		if (photoUpload != null && photoUpload.length > 0) {
			for (CommonsMultipartFile aFile : photoUpload) {

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
					fileName = aFile.getOriginalFilename();
					fileLocation = saveDirectory;
					
				}
			}
		}
		Blog persistentblog = blogservice.GetSpecificBlog(id);
		
		
		if (photoUpload != null && photoUpload.length > 0) {
		BlogPhoto photo = new BlogPhoto(fileName, fileLocation);
		persistentblog.setPhoto(photo);
		}
		
		blogservice.updateblog(persistentblog);

		return "redirect:/BlogList";
	}
	


}
