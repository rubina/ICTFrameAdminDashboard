package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.Events;
import com.softech.ictframe.DAOModels.EventsPhoto;
import com.softech.ictframe.DAOModels.Gadgets;
import com.softech.ictframe.DAOModels.GadgetsPhoto;
import com.softech.ictframe.services.GadgetService;

@Controller
public class GadgetsController {
	@Autowired
	private GadgetService gadgetService;
	public void setGadgetService(GadgetService GadgetService)
	{
		gadgetService=GadgetService;
	}
	@RequestMapping("/GadgetsList")
	public String showEventList(Model model)
	{
	List<Gadgets> gadgets=gadgetService.GetAllGadgets();
	model.addAttribute("gadgets", gadgets);
	return "GadgetList_admin";
	}
	@RequestMapping(value = { "/ViewGadgetsDetails-{id}-gadgets" }, method = RequestMethod.GET)
	public String showGadgetsDetails(Model model,@PathVariable int id,Gadgets gadget)
	{
		gadget=gadgetService.GetSpecificGadgets(id);
		model.addAttribute("gadget",gadget);
		return "ViewGadgetDetails";
	}

	@RequestMapping(value = { "/editGadgets-{id}-gadgets" }, method = RequestMethod.GET)
	public String showGadgetsToEdit(Model model,@PathVariable int id,Gadgets gadget)
	{
		gadget=gadgetService.GetSpecificGadgets(id);
		model.addAttribute("gadget",gadget);
		return "EditGadget_admin";
	}

	@RequestMapping(value = { "/editGadgets-{id}-gadgets" }, method = RequestMethod.POST)
	public String showUpdateGadget(Model model,Gadgets gadget,@PathVariable int id)
	{
		gadgetService.UpdateGadgets(gadget,id);
		return "ViewGadgetDetails";
	}
	@RequestMapping(value = { "/deleteGadgets-{id}-gadgets" })
	public String showDeleteGadgets(Gadgets gadget)
	{
		gadgetService.DeleteGadgetsById(gadget);
		
		return "redirect:/GadgetsList";
	}
	@RequestMapping("/GadgetsAdd")
	public String showGadgetAdd()
	{
	return "GadgetAdd_admin";	
	}
	@RequestMapping("/AddGadgets")
	public String showAddedEvent(Gadgets gadget,Model model)
	{
		gadgetService.SaveGadgets(gadget);
		int id=gadget.getId();
		model.addAttribute("id",id);
		return "GadgetsPhoto";
	}
	
	@RequestMapping(value = "/uploadGadgetsphoto")
	public String uploadGadgetsphoto(HttpServletRequest request, @RequestParam CommonsMultipartFile[] photoUpload,
		 @RequestParam("id") int id, Model model, Gadgets gadgets) throws Exception, IOException {

		String saveDirectory = "D:\\Spring\\ICTFrameAdminHibernate\\WebContent\\Resources\\images\\";
		String fileName = "";
		String fileLocation = "";	

		if (photoUpload != null && photoUpload.length > 0) {
			for (CommonsMultipartFile aFile : photoUpload) {

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
					fileName = aFile.getOriginalFilename();
					fileLocation = saveDirectory;
				}
			}
		}
		Gadgets persistentgadgets = gadgetService.GetSpecificGadgets(id);
		
		
		if (photoUpload != null && photoUpload.length > 0) {
		GadgetsPhoto photo = new GadgetsPhoto(fileName, fileLocation);
		persistentgadgets.setPhoto(photo);
		}
		
		gadgetService.updategadgets(persistentgadgets);

		return "redirect:/GadgetsList";
	}
	

}
