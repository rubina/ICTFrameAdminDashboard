package com.softech.ictframe.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.softech.ictframe.DAOModels.News;
import com.softech.ictframe.DAOModels.NewsPhoto;
import com.softech.ictframe.services.NewsService;


@Controller
public class NewsController {
@Autowired
private NewsService newsservice;
public void setNewsService(NewsService newssservice)
{
newsservice=newssservice;	
}
@RequestMapping("/NewsList")
public String showNewsList(Model model)
{
List<News> newss=newsservice.GetAllNewes();
model.addAttribute("news", newss);
return "NewsList_admin";
}
@RequestMapping(value = { "/ViewNewsDetails-{id}-news" }, method = RequestMethod.GET)
public String showNewsDetails(Model model,@PathVariable int id,News news)
{
	news=newsservice.GetSpecificNews(id);
	model.addAttribute("news",news);
	return "ViewNewsDetails";
}

@RequestMapping(value = { "/editNews-{id}-news" }, method = RequestMethod.GET)
public String showNewsToEdit(Model model,@PathVariable int id,News news)
{
	news=newsservice.GetSpecificNews(id);
	model.addAttribute("news",news);
	return "EditNews_admin";
}

@RequestMapping(value = { "/editNews-{id}-news" }, method = RequestMethod.POST)
public String showUpdateclass(Model model,News news,@PathVariable int id)
{
	newsservice.UpdateNews(news,id);
	return "ViewNewsDetails";
}
@RequestMapping(value = { "/deleteNews-{id}-news" })
public String showDeleteClass(News news)
{
	newsservice.DeleteClassById(news);
	
	return "redirect:/NewsList";
}
@RequestMapping("/NewsAdd")
public String showNewsAdd()
{
return "NewsAdd_admin";	
}
@RequestMapping("/Add")
public String showAddedNews(News news,Model model)
{
	newsservice.SaveNews(news);
	int id=news.getId();
		model.addAttribute("id",id);
	return "NewsPhoto";
}
@RequestMapping(value = "/uploadNewsPhoto")
public String uploadNewsPhoto(HttpServletRequest request, @RequestParam CommonsMultipartFile[] photoUpload,
	 @RequestParam("id") int id, Model model, News news) throws Exception, IOException {

	String saveDirectory = "D:\\Spring\\ICTFrameAdminHibernate\\WebContent\\Resources\\images\\";
	String fileName = "";
	String fileLocation = "";	

	if (photoUpload != null && photoUpload.length > 0) {
		for (CommonsMultipartFile aFile : photoUpload) {

			if (!aFile.getOriginalFilename().equals("")) {
				aFile.transferTo(new File(saveDirectory + aFile.getOriginalFilename()));
				fileName = aFile.getOriginalFilename();
				fileLocation = saveDirectory;
			}
		}
	}
	News persistentnews = newsservice.GetSpecificNews(id);
	
	
	if (photoUpload != null && photoUpload.length > 0) {
	NewsPhoto photo = new NewsPhoto(fileName, fileLocation);
	persistentnews.setPhoto(photo);
	}
	
	newsservice.updatenews(persistentnews);

	return "redirect:/NewsList";
}

}
