package com.softech.ictframe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ictframe.DAO.VideoDAO;
import com.softech.ictframe.DAOModels.Video;

@Service("videoservice")
public class VideoService {
	@Autowired
	private VideoDAO VideoDAO;
	public void setVideoDAO(VideoDAO Videosdao)
	{
		this.VideoDAO=Videosdao;
	}
	public void SaveVideo(Video Video)
	{
		VideoDAO.SaveVideo(Video);
	}
	public List<Video> GetAllVideo()
	{
		return VideoDAO.GetAllVideo();
	}
	public Video GetSpecificVideo(int id)
	{
		return VideoDAO.GetSpecificVideo(id);
	}
	public void UpdateVideo(Video Video,int id)
	{
		Video persistVideo=VideoDAO.GetSpecificVideo(id);
		persistVideo.setTitle(Video.getTitle());
		persistVideo.setDescription(Video.getDescription());
		persistVideo.setLink(Video.getLink());
		persistVideo.setDate(Video.getDate());
		persistVideo.setEditor(Video.getEditor());
		VideoDAO.UpdateVideo(persistVideo);
	}
	public void DeleteClassById(Video Video)
	{
	VideoDAO.Delete(Video);	
	}
	public void updateVideo(Video Video)
	{
		VideoDAO.UpdateVideo(Video);
	}

}
