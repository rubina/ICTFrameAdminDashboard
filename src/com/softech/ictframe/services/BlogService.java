package com.softech.ictframe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ictframe.DAO.BlogDAO;
import com.softech.ictframe.DAOModels.Blog;
@Service("blogservice")
public class BlogService {
	@Autowired
	private BlogDAO BlogDAO;
	public void setBlogDAO(BlogDAO Blogsdao)
	{
		this.BlogDAO=Blogsdao;
	}
	public void SaveBlog(Blog blog)
	{
		BlogDAO.SaveBlog(blog);
	}
	public List<Blog> GetAllBlog()
	{
		return BlogDAO.GetAllBlog();
	}
	public void DeleteBlog(Blog blog)
	{
		BlogDAO.DeleteBlog(blog);
	}
	public Blog GetSpecificBlog(int id)
	{
		return BlogDAO.GetSpecificBlog(id);
	}
	public void UpdateBlog(Blog blog ,int id)
	{
		Blog persistBlog=BlogDAO.GetSpecificBlog(id);
		persistBlog.setTitle(blog.getTitle());
		persistBlog.getPhoto();
		persistBlog.setDescription(blog.getDescription());
		persistBlog.setDate(blog.getDate());
		persistBlog.setEditor(blog.getEditor());
		BlogDAO.UpdateBlog(persistBlog);
	}
	public void DeleteBlogById(Blog blog)
	{
		BlogDAO.DeleteBlog(blog);
	}
	public void updateblog(Blog blog)
	{
		BlogDAO.UpdateBlog(blog);
	}
	

}
