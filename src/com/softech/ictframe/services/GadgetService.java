package com.softech.ictframe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ictframe.DAO.GadgetsDAO;
import com.softech.ictframe.DAOModels.Gadgets;
@Service("gadgetsservice")
public class GadgetService {
	@Autowired
	private GadgetsDAO gadgetsdao;
	public void setgadgetsdao(GadgetsDAO gadgetssdao)
	{
		this.gadgetsdao=gadgetssdao;
	}
	public void SaveGadgets(Gadgets gadgets)
	{
		gadgetsdao.SaveGadgets(gadgets);
	}
	public List<Gadgets> GetAllGadgets()
	{
		return gadgetsdao.GetAllGadgets();
	}
	public Gadgets GetSpecificGadgets(int id)
	{
		return gadgetsdao.GetSpecificGadgets(id);
	}
	public void UpdateGadgets(Gadgets gadgets,int id)
	{
		Gadgets persistGadgets=gadgetsdao.GetSpecificGadgets(id);
		persistGadgets.setTitle(gadgets.getTitle());
		persistGadgets.setDescription(gadgets.getDescription());
		persistGadgets.setDate(gadgets.getDate());
		persistGadgets.setEditor(gadgets.getEditor());
		gadgetsdao.UpdateGadgets(persistGadgets);
	}
	
	public void DeleteGadgetsById(Gadgets gadgets)
	{
		gadgetsdao.DeleteGadgets(gadgets);
	}
	public void updategadgets(Gadgets gadgets)
	{
		gadgetsdao.UpdateGadgets(gadgets);
	}
}
