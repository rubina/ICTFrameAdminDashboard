package com.softech.ictframe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ictframe.DAO.JobDAO;
import com.softech.ictframe.DAOModels.Job;
@Service("jobservices")
public class JobService {
	@Autowired
	private JobDAO jobDAO;
	public void setJobDAO(JobDAO jobdao)
	{
		this.jobDAO=jobdao;
	}
	public void SaveJob(Job job)
	{
		jobDAO.SaveJob(job);
	}
	public List<Job> GetAllJob()
	{
		return jobDAO.GetAllJob();
	}
	public Job GetSpecificJob(int id)
	{
		return jobDAO.GetSpecificJob(id);
	}
	public void UpdateJob(Job job,int id)
	{
		Job persistJob=jobDAO.GetSpecificJob(id);
		persistJob.setTitle(job.getTitle());
		persistJob.setDescription(job.getDescription());
		persistJob.setDate(job.getDate());
		persistJob.setEditor(job.getEditor());
		jobDAO.UpdateJob(persistJob);
	}
	public void DeleteJobById(Job job)
	{
	jobDAO.DeleteJob(job);	
	}
	public void updatejob(Job job)
	{
		jobDAO.UpdateJob(job);
	}

}
