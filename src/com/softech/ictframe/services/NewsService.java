package com.softech.ictframe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ictframe.DAO.NewsDAO;
import com.softech.ictframe.DAOModels.News;

@Service("newsservice")
public class NewsService {
	@Autowired
	private NewsDAO newsdao;
	public void setNewsDAO(NewsDAO newssdao)
	{
		this.newsdao=newssdao;
	}
	public void SaveNews(News news)
	{
		newsdao.SaveNews(news);
	}
	public List<News> GetAllNewes()
	{
		return newsdao.GetAllNews();
	}
	public News GetSpecificNews(int id)
	{
		return newsdao.GetSpecificNews(id);
	}
	public void UpdateNews(News news,int id)
	{
		News persistNews=newsdao.GetSpecificNews(id);
		persistNews.setTitle(news.getTitle());
		persistNews.setDescription(news.getDescription());
		persistNews.setDate(news.getDate());
		persistNews.setEditor(news.getEditor());
		newsdao.UpdateNews(persistNews);
	}
	public void DeleteClassById(News news)
	{
	newsdao.Delete(news);	
	}
	public void updatenews(News news)
	{
		newsdao.UpdateNews(news);
	}

}
