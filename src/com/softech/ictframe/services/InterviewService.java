package com.softech.ictframe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ictframe.DAO.InterviewDAO;
import com.softech.ictframe.DAOModels.Interview;
import com.softech.ictframe.DAOModels.News;
@Service("interviewservice")
public class InterviewService {
	@Autowired
	private InterviewDAO interviewDAO;
	public void setInterviewDAO(InterviewDAO interviewsdao)
	{
		this.interviewDAO=interviewsdao;
	}
	public void SaveInterview(Interview interview)
	{
		interviewDAO.SaveInterview(interview);
	}
	public List<Interview> GetAllInterview()
	{
		return interviewDAO.GetAllInterview();
	}
	public Interview GetSpecificInterview(int id)
	{
		return interviewDAO.GetSpecificInterview(id);
	}
	public void UpdateInterview(Interview interview,int id)
	{
		Interview persistInterview=interviewDAO.GetSpecificInterview(id);
		persistInterview.setTitle(interview.getTitle());
		persistInterview.setDescription(interview.getDescription());
		persistInterview.setDate(interview.getDate());
		persistInterview.setEditor(interview.getEditor());
		interviewDAO.UpdateInterview(persistInterview);
	}
	public void DeleteInterviewById(Interview interview)
	{
	interviewDAO.DeleteInterview(interview);	
	}
	
	public void updateinterview(Interview interview)
	{
		interviewDAO.UpdateInterview(interview);
	}

}
